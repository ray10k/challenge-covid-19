FROM covid-api:latest AS covid-api
RUN  echo "Running Covid-api Django" 
EXPOSE 8000
CMD python manage.py runserver 0.0.0.0:8000

FROM challenge-frontend:latest AS challenge
RUN  echo "Running nginx"
EXPOSE 9000
EXPOSE 5000
