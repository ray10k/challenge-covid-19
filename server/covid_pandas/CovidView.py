import pandas as pd
from covidmx import CovidMX
import json
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse, HttpResponseServerError

covid_dge_data = CovidMX(source='Serendipia').get_data()
data=json.loads(covid_dge_data.to_json(orient='records'))

def getData(request):
    return JsonResponse(data, safe=False)
