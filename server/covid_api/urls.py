from django.contrib import admin
from django.urls import path
from covid_pandas.CovidView import getData

urlpatterns = [
    path('api', getData),
]
