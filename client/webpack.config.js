const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
var webpack = require('webpack');

module.exports = {
    mode: 'development',
    target:'node',
    context: path.resolve(__dirname, '.'),
    entry:{
        "main": './src/js/main.js',
    } ,
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, './public/js/'),
    },
    plugins: [
        new CopyPlugin({
            patterns: [{
                    from: './src/js/choropleth.js',
                    to: 'dc.choropleth.js'
                },
            ],
            options: {
                concurrency: 100,
            },
        }),
    ]
    
};