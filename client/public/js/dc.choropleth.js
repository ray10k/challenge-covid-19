var GEO = {
    defaultWidth: 570,
    defaultHeight: 361,
    defaultwidthScale: 180,
    defaultScaleGaps: 8,
    defaultRatioScale: function () {
        return this.defaultwidthScale / this.defaultWidth
    },
    ratio: function () {
        return this.defaultWidth / this.defaultHeight
    },
    width: null,
    widthScale: null,
    scaleGaps: null,
    height: function () {
        return Math.floor(this.width / this.ratio())
    },
    setWidth: function (w) {
        if (typeof w == "number" && w > 0 && w < this.defaultWidth) {
            this.width = w;
            this.widthScale = w * this.defaultRatioScale();
            this.scaleGaps = (this.widthScale < 140) ? (this.widthScale < 110) ? 5 : 6 : 8;
        } else {
            this.width = +(this.defaultWidth);
            this.widthScale = +(this.defaultwidthScale);
            this.scaleGaps = +(this.defaultScaleGaps);
        }
        return this.width
    },

    projection: d3.geoMercator()
        .scale(1)
        .translate([0, 0]),
    path: function () {
        return d3.geoPath()
            .projection(this.projection);
    },
    JSON: null,
    setJSON: function (JSON) {
        this.JSON = JSON
    },
    bounds: function () {
        return this.path().bounds(this.JSON);
    },
    scale: function () {
        let b = this.bounds(this.JSON);
        let w = this.width;
        let h = this.height();
        let s = .95 / Math.max((b[1][0] - b[0][0]) / w, (b[1][1] - b[0][1]) / h);
        return s;
        // return .95 / Math.max((this.bounds()[1][0] - this.bounds()[0][0]) / this.width, (this.bounds()[1][1] - this.bounds()[0][1]) / this.height())
    },
    translate: function () {
        return [(this.width - this.scale() * (this.bounds()[1][0] + this.bounds()[0][0])) / 2, (this.height() - this.scale() * (this.bounds()[1][1] + this.bounds()[0][1])) / 2]
    },
    adjustProjection: function () {
        // console.log(this.width);
        // console.log(this.height());
        // console.log(this.ratio());
        // console.log(this.JSON);
        // console.log("llllllllllllll");
        // console.group("scale & translate");
        // console.log(  "scale", this.scale()
        //             , "translate", this.translate());
        // console.group("size");
        // console.log('JSON',this.JSON
        //     , "bounds", this.bounds()
        //     , "width: ", this.width
        //     , "height", this.height());
        // console.groupEnd();
        // console.groupEnd();





        return this.projection.scale(this.scale()).translate(this.translate());
    }

}

function makeChoropleth(divID, crossDimension, crossGroup, geoJSON, optionalParameter) {
    // *Remove maxDomainValue ... value / 1.4
    let maxDomainValue = typeof optionalParameter.maxDomainValue === 'undefined' ? crossGroup.top(1)[0].value : optionalParameter.maxDomainValue;
    let divIDwidth = parseInt(d3.select(divID).style("width"));
    let geoUtils = GEO;
    console.log(geoUtils.setWidth(divIDwidth));
    console.log(geoUtils.height());

    // console.log(GEO.width);
    // console.log(GEO.ratio());
    // console.log(GEO.height());
    geoUtils.setJSON(geoJSON);

    choroplethRange = d3.scalePow().exponent(0.27)
        .domain([0, maxDomainValue])
        .range([0, 1]);

    chart = dc.geoChoroplethChart(divID);

    chart.type = "choropleth";
    chart
        .width(geoUtils.width)
        .height(geoUtils.height())
        .dimension(crossDimension)
        .group(crossGroup)
        .projection(geoUtils.adjustProjection())
        // .projection(projection)
        // colors(d3.interpolateViridis).
        .colorDomain([0, maxDomainValue])
        .colorCalculator(function (d) {
            return (typeof d !== "undefined") ?
                (d !== 0) ?
                d3.interpolateYlGnBu(choroplethRange(d)) : "#333" :
                '#000';
        })
        .overlayGeoJson(geoJSON.features, "states", function (d) {
            if (d.properties.OID > 0) {
                return d.properties.OID + "";
            }
        })
        .valueAccessor(function (kv) {
            return kv.value;
        })
        .useViewBoxResizing(true);
    //.title(optionalParameter.title);


    chart.on("renderlet", function (_chart) {
        _gaps = geoUtils.scaleGaps;
        _scalewidth = geoUtils.widthScale;
        _scale = d3.scalePow().exponent(0.27);
        _t = d3.scalePow().exponent(6).domain([0, _gaps]).range([0, 1]);
        _p = _scale.domain([0, 1]).range([0, maxDomainValue])
        _axisScale = d3.scaleLinear().domain([0, maxDomainValue]).range([0, _scalewidth]);
        _tempScale = d3.range(_gaps).map((v, i) => _p(_t(i + 1))); //(v,i)=> _t(i))
        // xAxis = d3.axisBottom(d3.scalePow().exponent(0.27).domain([0, maxDomainValue]).range([0,_scalewidth]))
        //             // .tickSize(5)
        //             .tickValues(_tempScale)
        //             ;


        choroplethAxis = d3.axisBottom(_axisScale)
            .tickValues(_tempScale);
        g = _chart.svg().append("g")
            .attr("class", "key")
            .attr("transform", "translate(40," + (geoUtils.height() - 35) + ")");
        g.append("g")
            .selectAll('.bbb')
            .data(_tempScale.map(function (v, i) {
                return {
                    'key': i,
                    'value': v
                }
            })).enter()
            .append('rect')
            .attr("x", function (d) {
                return _axisScale(d.key - 1 >= 0 ? _tempScale[d.key - 1] : 0)
            })
            .attr("width", function (d) {
                return _axisScale(d.key - 1 >= 0 ? d.value - _tempScale[d.key - 1] : d.value);
            })
            .attr("height", 15)
            .style("fill", function (d) {
                return d3.interpolateYlGnBu(choroplethRange(d.value));
            });

        g.append("g")
            .attr("transform", "translate(0,15)")
            .call(choroplethAxis);
    });
    return chart;

}
