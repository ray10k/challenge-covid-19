Repositorio para el challenge Covid-19
=====

Este repositorio se creo como parte del challenge Covid-19 en Mexico, el cual se constituia de 4 pasos, los cuales eran:

1. Desarrollar un front-end para el API creado por [Federico Garza:covidmx](https://github.com/FedericoGarza/covidmx) el cual contenia los datos del servidor de Serendipia, los campos a analizar eran los siguientes.

    1. Edad
    2. Sexo
    3. Fecha de inicio de los sintomas
    4. Resultado de la prueba PCR
    5. Estado de la republica del paciente.
    
     Para desarrollar esta aplicacion se usaron una serie de librerias para la creación de los gráficos, principalmente se encuentran las siguientes:

    - D3JS
    - Crossfilter
    - DCJS

    Ya que el JS tenia un peso aproximado de 130 MB a la fecha de 23/Sep/2020, se optó por organizar y analizar los datos en la computadora del cliente, sabiendo todo lo que esto conlleva, sin embargo, una de las ventajas se encuentran, es la capacidad de contar una historia mas adecuada a las necesidades de cada usuario.

    Ya que todas las graficas estan relacionadas entre si, por ejemplo, se puede correlacionar tiempo, resultados, edad y estado sin ningun problema.

    Cada Grafica tiene operadores 'OR' consigo mismas y operadores 'AND' con las demas. Esto puede ayudar a filtrar y conocer con mas claridad la situación de cada uno de los estados.

2. El punto 2 de crear una imagen de Docker se hizo previo al desarrollo del dashboard esto es porque implicaba que los contenedores tenian que comunicarse entre si, dado que se deseaba una imagen construida como tal. creo la imagen con el siguiente comando

```shell
$ cd $PATH2DIR/challenge-covid-master/client
$ docker build -t challenge-frontend .
```

para que los contenedores se conecten creé una red local dentro de los contendores, en mi caso _--network=netx_ pero es suficiente con que se encuentren en la misma red.

```shell
$ docker network create netx
```

para que los contenedores se conecten a la hora de levantar el servicio con el comando

```shell
$ docker run -it --rm -p 8000:8000 --net=netx --name covid-api covid-api

$ docker run -it --rm  -p 9000:80 --net=netx challenge-frontend
```

Esto para que dentro del proxy inverso del contenedor de frontend pueda llamar a el contendor con la direccion _http:covid-api:8000/backend_

3. Este punto quedo solucionado al implemental la forma descrita en el punto anterior al hacer 2 imagenes, ya que se puede acceder con el proxy inverso a la imagen de _DJANGO_ 
    - _localhost:9000/_
    - _localhost:9000/backend/api_

Si se cambia el puerto a 5000 o se añade una redireccion de puertos extra queda como deseaban.


4. Se soluciono la conexion al cluster de kubernetes, conectandome con el _port-forward_ .
de forma que la instalacion queda con los siguientes pasos:

------

## Requerimientos
1. Docker
2. Kubernetes
3. kind (K8ts)




## Instalacion

Para instalar el challenge lo que se tiene que hacer es lo siguiente
Primero cambiar al directorio del repositorio.

```shell
cd path/to/git
```
y ejecutar los siguientes comandos (use Chaluxa como namespace pero puede usar el que mas les convenga):

```shell
kind create cluster --name chaluxa --config ./.kubernetes/kind.yaml
```

ya una ves creado el cluster se sigue el manual de quickstart de kind:

```shell
docker build -t covid-api:1 ./server
kind load docker-image covid-api:1 --name chaluxa
kubectl apply -f ./.kubernetes/back/manifest/
```
Se hace el proceso analogo para el _frontend_:

```shell
docker build -t covid-frontend:1 ./client
kind load docker-image covid-frontend:1 --name chaluxa
kubectl apply -f ./.kubernetes/front/manifest/
```

una vez terminado los anteriores procesos se accede al servicio/


```shell
kubectl port-forward service/frontend 9000:9000
```
donde el puerto puede ser el 5000 si asi lo desean.





 
  